#!/usr/bin/env bash
# For open files using inet
#
# See also: https://linux-audit.com/cheat-sheets/lsof/

if command -v lsof >/dev/null 2>&1; then
	Alias 'lsof'		'lsof'	root
	Alias 'lsinet'		'lsof -ni'
	Alias 'lsinet'		'echo "Can try socstat-xxx (ss) instead also...";' pre
	Alias 'lsinet-tcp'	'lsinet TCP'
	Alias 'lsinet-udp'	'lsinet UDP'
	Alias 'lsArma3ds'	'lsof -nwP +r 1m====%T==== -a -c arma3server -i'

	# To only list ports of interest...
	{
		ports=(
			80		# Web: HTTP
			443		# Web: HTTPS
			9050	# Tor: SocksPort
			9051	# Tor: ControlPort
			9053	# Tor: DNSPort
			9080	# Tor: HTTPTunnelPort
			9081	# Tor: HTTP TransPort
			3000-3001	# Nodejs: servers (default)
			8080-8081	# Nodejs: servers (alt)
		)
		# Convert the array to a single string,
		printf -v ports "%s" "${ports[*]}"
		# with commas between, and a ':' prepended
		printf -v ports ":%s" "${ports// /,}"
	}
	Alias 'ls-PortsOfInterest'		'lsinet '"${ports}"
	# Do NOT use the tcp/udp aliased versions from above inside,
	# it wont work somehow ¯\_(ツ)_/¯
	Alias 'ls-tcp-PortsOfInterest'	'lsinet TCP'"${ports}"' -sTCP:LISTEN,ESTABLISHED'
	Alias 'ls-udp-PortsOfInterest'	'lsinet UDP'"${ports}"
	unset ports
fi