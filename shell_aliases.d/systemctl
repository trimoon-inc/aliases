#!/usr/bin/env bash

# shellcheck disable=2034
opts_sc=(
# 	--output=short-monotonic
	--lines=25
	--no-pager
	--full
	--show-transaction
)
# shellcheck disable=2016
Alias 'sc'				'systemctl "${opts_sc[@]}"'
Alias '_sc'				'sc' root

## Unit Commands (Introspection and Modification)
Alias 'sc-status'				'sc status'
Alias 'sc-status-with-dependencies'			'sc-status --with-dependencies'
Alias 'sc-status-with-dependencies-after'	'sc-status-with-dependencies --after'
Alias 'sc-status-with-dependencies-before'	'sc-status-with-dependencies --before'
Alias 'sc-show'					'sc show'
Alias 'sc-cat'					'sc cat'
Alias 'sc-help'					'sc help'
Alias 'sc-kill-signal'			'sc kill --signal'
Alias 'sc-link'					'_sc link'
Alias 'sc-list-dependencies'		'sc list-dependencies'
Alias 'sc-list-dependencies-after'	'sc-list-dependencies --after'
Alias 'sc-list-dependencies-before'	'sc-list-dependencies --before'
Alias 'sc-list-jobs'			'sc list-jobs'
Alias 'sc-list-timers'			'sc list-timers'
Alias 'sc-list-units'			'sc list-units'
Alias 'sc-service-log-level'	'sc service-log-level'
Alias 'sc-start'				'_sc start'
Alias 'sc-stop'					'_sc stop'
Alias 'sc-reload'				'_sc reload'
Alias 'sc-reset-failed'			'sc reset-failed'
Alias 'sc-restart'				'_sc restart'
## Unit File Commands
Alias 'sc-edit'					'sc edit'
Alias 'sc-enable'				'_sc enable'
Alias 'sc-enable-now'			'sc-enable --now'
Alias 'sc-disable'				'_sc disable'
Alias 'sc-disable-now'			'sc-disable --now'
Alias 'sc-list-unit-files'		'sc list-unit-files'
Alias 'sc-reenable'				'_sc reenable'
Alias 'sc-reenable-now'			'sc-reenable --now'
Alias 'sc-preset'				'sc preset'
Alias 'sc-mask'					'_sc mask'
Alias 'sc-mask-now'				'sc-mask --now'
Alias 'sc-unmask'				'sc unmask'
Alias 'sc-unmask-now'			'sc-unmask --now'
Alias 'sc-revert'				'sc revert'
## Manager State Commands
Alias 'sc-daemon-reload'		'sc daemon-reload'
## System Commands
Alias 'sc-rescue'				'_sc rescue'
Alias 'sc-emergency'			'_sc emergency'
Alias 'sc-poweroff'				'sync && _sc poweroff --message="Admin requested"'
Alias 'sc-reboot'				'sync && _sc reboot --message="Admin requested"'

# systemctl User manager
Alias 'scuser'					'sc --user'
## Unit Commands (Introspection and Modification)
Alias 'sc-user-status'				'scuser status'
Alias 'sc-user-status-with-dependencies'		'sc-user-status --with-dependencies'
Alias 'sc-user-status-with-dependencies-after'	'sc-user-status-with-dependencies --after'
Alias 'sc-user-status-with-dependencies-before'	'sc-user-status-with-dependencies --before'
Alias 'sc-user-show'				'scuser show'
Alias 'sc-user-cat'					'scuser cat'
Alias 'sc-user-help'				'scuser help'
Alias 'sc-user-kill-signal'			'scuser kill --signal'
Alias 'sc-user-list-dependencies'			'scuser list-dependencies'
Alias 'sc-user-list-dependencies-after'		'sc-user-list-dependencies --after'
Alias 'sc-user-list-dependencies-before'	'sc-user-list-dependencies --before'
Alias 'sc-user-list-jobs'			'scuser list-jobs'
Alias 'sc-user-list-timers'			'scuser list-timers'
Alias 'sc-user-list-units'			'scuser list-units'
Alias 'sc-user-service-log-level'	'scuser service-log-level'
Alias 'sc-user-start'				'scuser start'
Alias 'sc-user-stop'				'scuser stop'
Alias 'sc-user-reset-failed'		'scuser reset-failed'
Alias 'sc-user-restart'				'scuser restart'
## Unit File Commands
Alias 'sc-user-enable'				'scuser enable'
Alias 'sc-user-enable-now'			'sc-user-enable --now'
Alias 'sc-user-disable'				'scuser disable'
Alias 'sc-user-disable-now'			'sc-user-disable --now'
Alias 'sc-user-list-unit-files'		'scuser list-unit-files'
Alias 'sc-user-reenable'			'scuser reenable'
Alias 'sc-user-reenable-now'		'sc-user-reenable --now'
## Manager State Commands
Alias 'sc-user-daemon-reload'		'scuser daemon-reload'

# systemctl Global manager (all users)
# Missing entries wrt user means that there is no global version
#	and you need to use the user version instead...
Alias 'sc-global'				'_sc --global'
## Unit File Commands
Alias 'sc-global-enable'			'sc-global enable'
Alias 'sc-global-enable-now'		'sc-global-enable --now'
Alias 'sc-global-disable'			'sc-global disable'
Alias 'sc-global-disable-now'		'sc-global-disable --now'
Alias 'sc-global-link'				'sc-global link'
Alias 'sc-global-list-unit-files'	'sc-global list-unit-files'
Alias 'sc-global-reenable'			'sc-global reenable'
Alias 'sc-global-reenable-now'		'sc-global-reenable --now'

_sc_mount_status () {
	local units=()
	for unit in "$@"; do
		units+=( "${unit}".{auto,}mount )
	done
	_sc status "${units[@]}"
}
Alias 'sc-mount-status'			'_sc_mount_status'