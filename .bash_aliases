if [ -f ~adm/shell_aliases ]; then
	. ~adm/shell_aliases
fi

if test -f ~adm/complete_alias/complete_alias; then
	source ~adm/complete_alias/complete_alias
	# complete all my aliases
	complete -F _complete_alias "${!BASH_ALIASES[@]}"
fi