# export SHELL = /usr/bin/bash

all: initClone

initClone:
	setfacl --recursive --remove-all .
	setfacl --recursive --modify g:adm:rwX,g:sudo:rwX,d:g:adm:rwX,d:g:sudo:rwX .
	for l in setAlias shell_aliases* .bash_aliases; do ln -sfnr $$l ~adm/$$l; done