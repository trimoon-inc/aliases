# About:

Common shell `alias`es we use 😉

This repo makes use of **submodules**, so clone using the `--recurse-submodules` option !

```console
git clone --recurse-submodules https://gitlab.com/trimoon-inc/aliases.git
```

## Usage:

1. _Create the `adm` user if needed, but most distros have it by default._

1. Set the **HOME** directory for the `adm` user to `/home/adm` or whatever suits you better.

1. Make sure the `adm` user has disabled logins to make it more secure, fe. by using `/usr/sbin/nologin` as login shell.

1. ⚠️ After cloning the repo:
	- Make sure you perform the below _(if needed)_ in the directory as `root` where the repo was cloned into:

		```
		setfacl --recursive --modify g:adm:rwX,g:sudo:rwX,d:g:adm:rwX,d:g:sudo:rwX .
		```

1. Symbolic link `setAlias`, `shell_aliases.d`, `shell_aliases` and `.bash_aliases` from the repo into `~adm/`

	```
	for l in setAlias shell_aliases* .bash_aliases; do ln -sfnr $l ~adm/$l; done
	```

1. To auto load the aliases, Either:
   - Symbolic link your personal `.bash_aliases` to the `.bash_aliases` file in the repo.
   - Manually add the below to your personal `.bash_aliases` file as last commands:

		```bash
		if [ -f ~adm/shell_aliases ]; then
			. ~adm/shell_aliases
		fi
		```

## Submodules:

This repo makes use of the [setAlias] submodule !

These git-configs will assist you in development/changes to the code(s):

1. `push.recursesubmodules=on-demand`

1. `diff.submodule=log`

1. `status.submodulesummary=true`

1. `submodule.recurse=true`

See: [gitBook: Submodules]

[setAlias]: https://gitlab.com/trimoon-inc/setalias
[gitBook: Submodules]: https://git-scm.com/book/en/v2/Git-Tools-Submodules